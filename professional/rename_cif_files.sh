#!/usr/bin/env sh

# Author: Christian Schreinemachers (Cs137)
# License: MIT

# This script serves to rename all CIF files in a given folder.

# The filename is generated based on the CIF files content as follows:
#   new_name="${chemical_composition}_${space_group}_${author}.cif"
# It has its limitations and will not work for all CIF files


# Check if the "--help" or "-h" flag is set
if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
  echo "This script renames all CIF files in a given directory based on their content"
  echo "Usage: rename_cif_files [-v] <directory>"
  echo "-v: display messages showing the renaming process"
  exit 0
fi

# The verbose flag, which determines whether messages should be displayed
verbose=false

# Check if the verbose flag is set
if [ "$1" == "-v" ]; then
  verbose=true
  # The directory containing the CIF files is the second argument
  dir="$2"
else
  # The directory containing the CIF files is the first argument
  dir="$1"
fi


# Display a start message if the verbose flag is set
if [ "$verbose" = true ]; then
  echo "Renaming CIF files in $dir"
fi

# Loop through all CIF files in the directory
for file in $dir/*.cif; do
  # Extract the chemical composition, space group, and author from the file
  chemical_composition=$(grep "chemical_formula_sum" $file | awk '{print $2}')
  space_group=$(grep "space_group_name_H-M" $file | awk '{print $2}')
  author=$(grep "author" $file | awk '{print $2}')

  # Construct the new file name using the extracted information
  new_name="${chemical_composition}_${space_group}_${author}.cif"

  # Display a message if the verbose flag is set
  if [ "$verbose" = true ]; then
    echo "Renaming $file to $new_name"
  fi

  # Rename the file using the new name
  mv "$file" "$dir/$new_name"
done

# Display an end message if the verbose flag is set
if [ "$verbose" = true ]; then
  echo "Finished renaming CIF files in $dir"
fi
