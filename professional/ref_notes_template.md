---

title:      {{ title }}
author:     {{ author }}
date:       {{ date }}

bibtex_key: {{ bibtex_key }}
keywords:   [{{ keywords }}]

---

## Literature details

Title: __{{ ref_title }}__  
Author(s): {{ ref_author }}  
Year: __{{ ref_year }}__,
DOI: {{ ref_doi }}

### Abstract

{{  ref_abstract|default('[No abstract in BibTeX database]')  }}


## Summary

[Brief summary of the paper]


## Key points

- [List of key points or takeaways from the paper]


## Notes

[Additional notes or thoughts on the paper]
