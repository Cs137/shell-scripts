#!/usr/bin/env python

# Author: Christian Schreinemachers (Cs137)
# License: MIT

import datetime
import os
import subprocess

import bibtexparser
import click
import jinja2


AUTHOR = "Christian Schreinemachers"
NOTES_DIR = os.path.join(os.getenv("HOME"), "Documents/Notes/Literature")
NOTES_PREFIX = "notes_"
NOTES_SUFFIX = ".md"
ENCODING = "utf-8"
TEMPLATE = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "ref_notes_template.md")
)
if os.getenv("BIB"):
    BIB_FILE = os.getenv("BIB")
else:
    BIB_FILE = None
if os.getenv("EDITOR"):
    EDITOR = os.getenv("EDITOR")
else:
    EDITOR = "nano"


def get_bibtex_database(
    file_path: str, encoding=ENCODING
) -> bibtexparser.bibdatabase.BibDatabase:
    """
    Load a BibTeX database from a bib file.

    :param file_path: Path to the BibTeX database file.
    :param encoding:  Encoding of the BibTeX file.
    :return:          BibTeX database object.
    """
    with open(file_path, encoding=encoding) as bibtex_file:
        return bibtexparser.bparser.BibTexParser(common_strings=True).parse_file(
            bibtex_file
        )


def open_notes(file_path: str) -> None:
    """
    Open a reference notes file.

    :param file_path: Path to the notes file.
    """
    subprocess.run([EDITOR, file_path])


def create_notes(
    entry: dict[str, str], file_path: str, author: str, template: str, encoding=ENCODING
) -> None:
    """
    Create a reference notes file for a BibTeX key.

    :param entry:     BibTeX entry as dictionary.
    :param file_path: Path to the notes file.
    :param author:    Author of the notes.
    :param template:  Path to the Jinja2 notes template file.
    :param encoding:  Encoding of the notes file.
    """
    # Load template
    loader = jinja2.FileSystemLoader(os.path.dirname(template))
    env = jinja2.Environment(loader=loader)
    template = env.get_template(os.path.basename(template))
    # Assign field values
    values = {
        "title": f"Notes on Literature: {entry['ID']}",
        "author": author,
        "date": datetime.date.today(),
        "bibtex_key": entry["ID"],
        "ref_title": entry["title"],
        "ref_author": entry["author"],
        "ref_year": entry["year"],
        "ref_doi": f"[{entry['doi']}](https://doi.org/{entry['doi']})",
    }
    if "keywords" in entry.keys():
        values["keywords"] = entry["keywords"]
    if "abstract" in entry.keys():
        values["ref_abstract"] = f"> {entry['abstract']}"
    # Create and write content
    notes_content = template.render(**values)
    with open(file_path, "w", encoding=encoding) as fobj:
        fobj.write(notes_content)


@click.command()
@click.argument("key")
@click.option(
    "-a",
    "--author",
    default=AUTHOR,
    show_default=True,
    type=click.STRING,
    help="Author of notes.",
)
@click.option(
    "-b",
    "--bibtex-file",
    default=BIB_FILE,
    show_default=True,
    type=click.Path(exists=True),
    help="Path to existing BibTeX database file.",
)
@click.option(
    "-d",
    "--directory",
    default=NOTES_DIR,
    show_default=True,
    type=click.Path(exists=False),
    help="Reference notes directory.",
)
@click.option(
    "-p",
    "--prefix",
    default=NOTES_PREFIX,
    show_default=True,
    type=click.STRING,
    help="Prefix for notes filename.",
)
@click.option(
    "-s",
    "--suffix",
    default=NOTES_SUFFIX,
    show_default=True,
    type=click.STRING,
    help="Suffix for notes filename.",
)
@click.option(
    "-e",
    "--encoding",
    default=ENCODING,
    show_default=True,
    type=click.STRING,
    help="Encoding for the BibTeX and markdown files.",
)
@click.option(
    "-t",
    "--template",
    default=TEMPLATE,
    show_default=True,
    type=click.Path(exists=True),
    help="Path to existing template file.",
)
@click.help_option("--help", "-h")
def main(
    key: str,
    author: str,
    directory: str,
    bibtex_file: str,
    prefix: str,
    suffix: str,
    encoding: str,
    template: str,
) -> None:
    """(Create and) Open reference notes file

    The notes file is a markdown file located in the directory declared as
    argument, if none is provided, a hardcoded path from the script is used.
    The filename corresponds to {prefix}{key}{suffix} and it is opened with
    the editor set to the environmental variable EDITOR (fallback: nano).

    If a new file is created, a check is performed to verify that the key
    is registered in the BibTeX database and certain metadata are inserted
    into the provided template file. If no database file is specified, the
    value set to the environmental variable BIB is used for that purpose.
    """

    # Generate file path from directory, prefix, key, and suffix
    file_path = os.path.join(directory, prefix + key + suffix)

    if not os.path.exists(file_path):
        # Load bibtex database
        click.echo(f"Loading BibTeX database from '{os.path.basename(bibtex_file)}'...")
        if bibtex_file is None:
            raise ValueError("No BibTeX database file specified.")
        bib_database = get_bibtex_database(bibtex_file, encoding=encoding)

        # Load corresponding bibtex entry
        click.echo(f"Loading entry '{key}' from BibTeX database...")
        if key not in bib_database.entries_dict.keys():
            raise ValueError(
                f"'{key}' not existing in '{os.path.basename(bibtex_file)}.'"
            )
        entry = bib_database.entries_dict[key]

        # Create notes directory if its not existing
        if not os.path.exists(directory):
            click.confirm(
                f"Create literature notes directory '{directory}'?",
                default=True,
                abort=True,
            )
            os.makedirs(directory)
            click.echo(f"Created directory: '{directory}'")

        # Create the notes file
        create_notes(entry, file_path, author, template, encoding=encoding)
        click.echo(f"Created literature notes: '{os.path.basename(file_path)}'")

    open_notes(file_path)


if __name__ == "__main__":
    main()
