#!/usr/bin/env python

"""
BibTeX Names to Initials Conversion Script

Usage:
    bib_names2initials.py <input_bib_file>

This script converts the first names in author names of a BibTeX file to initials.

Arguments:
    <input_bib_file>: Path to the input BibTeX file to be modified.

Example:
    bib_names2initials.py your_bib_file.bib
"""

import sys

try:
    import bibtexparser
except ImportError:
    print("Error: 'bibtexparser' module not found.")
    print("Please install it using the following command:")
    print("pip install bibtexparser")
    sys.exit(1)

# Constant for file encoding
ENCODING = "utf-8"


def convert_names_to_initials(entry, print_conversion=True) -> int:
    """
    Convert first names in author names to initials.

    Parameters:
        entry (dict): BibTeX entry with 'author' field to be modified.
        print_conversion (bool): Flag to control whether to print conversion statements.
    """
    converted_count = 0
    if " and " in entry["author"]:
        authors = entry["author"].split(" and ")
    else:
        authors = [entry["author"]]

    converted_authors = []
    for author in authors:
        # print(author)
        name_parts = author.split(",")
        # print(name_parts)
        lastname, firstnames = name_parts[0], name_parts[1]
        firstnames = firstnames.split()
        converted_firstnames = [
            f"{name.strip()[0]}." if "." not in name else name for name in firstnames
        ]
        if converted_firstnames != firstnames:
            converted_count += 1
            if print_conversion:
                print(f"> {lastname}: {firstnames} => {converted_firstnames}")
        converted_authors.append(f"{lastname}, {' '.join(converted_firstnames)}")

    entry["author"] = " and ".join(converted_authors)
    return converted_count


def main():
    """
    Main function to execute the BibTeX name conversion.
    """
    if len(sys.argv) != 2:
        print("Usage: bib_names2initials.py <input_bib_file>")
        sys.exit(1)

    bib_file_path = sys.argv[1]

    # Load the BibTeX file
    with open(bib_file_path, "r", encoding=ENCODING) as bibtex_file:
        bib_database = bibtexparser.load(bibtex_file)

    # Process each entry in the BibTeX file
    total_converted_count = 0
    for entry in bib_database.entries:
        converted_count = convert_names_to_initials(entry, print_conversion=True)
        total_converted_count += converted_count
    print(f"Total names converted: {total_converted_count}")

    if total_converted_count > 0:
        print(
            f"Conversion completed. File '{bib_file_path}' overwritten with modified content."
        )

        # Save the modified BibTeX file, overwriting the input file
        with open(bib_file_path, "w", encoding=ENCODING) as output_file:
            bibtexparser.dump(bib_database, output_file)


if __name__ == "__main__":
    main()
