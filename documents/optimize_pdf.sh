#!/usr/bin/env sh

# This script was created by Christian Schreinemachers (Cs137)
# License: MIT

# Short description: This script optimizes a PDF file by reducing its file size
# using Ghostscript.

# Parse the command-line arguments
while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      # Display usage information
      echo "Usage: optimize_pdf.sh <file> [-s <suffix>]"
      echo "  -s, --suffix  The suffix to use for the optimized PDF file (default: _opt)"
      exit 0
      ;;
    -s|--suffix)
      # Set the suffix
      suffix="$2"
      shift 2
      ;;
    *)
      # Set the input file
      file="$1"
      shift 1
      ;;
  esac
done

# Set the default suffix if not specified
if [ -z "$suffix" ]; then
  suffix="_opt"
fi

# Get the full path of the input file and its directory
file=$(readlink -f "$file")
dir=$(dirname "$file")

# Get the base name of the input file without the extension
base="${file%.*}"

# Optimize the input file and save it with the specified suffix
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile="$base$suffix".pdf $file
