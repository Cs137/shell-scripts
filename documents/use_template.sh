#!/usr/bin/env sh

# This script was created by Christian Schreinemachers (Cs137)
# License: MIT


# Set the default values for the script arguments
template_root_dir=""
destination_dir=$(pwd)

# Use the default template root directory if the -t or --template-dir option is not present
template_root_dir=${TEMPLATE_ROOT_DIR:-$template_root_dir}

# Parse the command-line arguments using getopts
while getopts "ht:d:" opt; do
  case $opt in
    h)
      # Display the usage instructions and script description if the -h or --help option is present
      echo "Usage: $0 [-h|--help] [-t|--template-dir] [directory] [-d|--destination] [directory]"
      echo ""
      echo "This script creates a copy of a selected template folder and renames it with the specified project name."
      exit 0
      ;;
    t)
      # Set the template root directory if the -t or --template-dir option is present
      template_root_dir=$OPTARG
      ;;
    d)
      # Set the destination directory if the -d or --destination option is present
      destination_dir=$OPTARG
      ;;
    \?)
      # Display an error message and exit if an invalid option is present
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

# Check if the template root directory exists and is a directory
if [ ! -d "$template_root_dir" ]; then
  # Display an error message and exit if the template root directory does not exist or is not a directory
  echo "The template root directory does not exist or is not a directory." >&2
  exit 1
fi

# Check if the destination directory exists and is a directory
if [ ! -d "$destination_dir" ]; then
  # Display an error message and exit if the destination directory does not exist or is not a directory
  echo "The destination directory does not exist or is not a directory." >&2
  exit 1
fi

template_folders=$(find $template_root_dir -type d -printf "%h/%f\n")

# Create a new array to hold the template folder names
template_names=()

# Loop through the template folders and extract the template names
for template_folder in ${template_folders[@]}
do
    # Extract the template name from the directory name
    template_name=$(basename $template_folder)

    # Add the template name to the array
    template_names+=($template_name)
done

# Use the select command to display a menu of templates
select template_name in ${template_names[@]}
do
    # Use the find command to search for the selected template folder in the template root directory
    # The -printf option is used to print the full path to the selected template folder
    template_folder=$(find $template_root_dir -type d -name $template_name -printf "%h/%f\n")

    # Prompt the user for the name of the new project
    echo "Enter the name of the new project:"
    read project_name

    # Replace any white space in the project name with an underscore
    project_name=$(echo $project_name | sed -e 's/ /_/g')

    # Create the project directory
    mkdir $project_name

    # Copy the selected template folder to the project directory
    cp -r $template_folder/* "${destination_dir}/${project_name}"

    # Use the find command to search for the main LaTeX file in the copied template folder
    # The -exec option is used to rename the main LaTeX file with the project name
    find "${destination_dir}/${project_name}" -name "template.tex" -o -name "template.md" -exec mv {} "${destination_dir}/${project_name}/${project_name}.md" \;


    break
done
