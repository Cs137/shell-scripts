# Shell Scripts

This repository contains a collection of shell scripts that can be used for various purposes.
The scripts are organized into different folders, depending on their intended use.


## Directory Structure

```
.
├── administration
├── development
├── documents
└── professional
```


## Installation

To install the scripts from this repository, follow these steps:

1. Clone the repository to your local machine using Git:

    ```shell
    git clone https://codeberg.org/Cs137/shell-scripts.git
    ```

2. Change into the directory of the cloned repository:

    ```shell
    cd shell-scripts
    ```

3. Make the scripts executable:

    ```shell
    find . -name "*.sh" -or -name "*.py" -type f -exec chmod -v u+x {} \;
    ```

4. Add the scripts to your `PATH` so that you can run them from any directory:

    ```shell
    export PATH=$PATH:/path/to/scripts
    ```

    - Alternatively, you can create symbolic links to the scripts in a directory that is already in your `PATH`, such as `/usr/local/bin`:

      ```shell
      ln -s /path/to/scripts/mk_script.sh /usr/local/bin/mk_script
      ln -s /path/to/scripts/use_template.sh /usr/local/bin/use_template
      ```

      To make the changes to your `PATH` or create symbolic links persistently, you can add the `export` or `ln` commands to your shell profile or `.bashrc`.

    - Another alternative is to use the rules defined in the `Makefile` in order to `link` or `unlink` the scripts to `~/.local/bin/tools`.
    The links are created with [GNU Stow](https://www.gnu.org/software/stow/) and the destination is hard-coded in the [`Makefile`](Makefile).

      Create links:

      ```shell
      make link
      ```

      Remove links:

      ```shell
      make unlink
      ```


## Usage

This repository contains the following scripts:


### administration

#### mk_script.sh

This script allows users to create a new shell script with a specified filename, author, and license.

To use the script, run the following command:

```
./mk_script.sh <filename> [-a <author>] [-l <license>] [-d <destination>]
```

For example:

```
./mk_script.sh my_script.sh -a "John Doe" -l MIT -d /home/john/scripts
```

This command will create a new shell script called my_script.sh with the specified author, license, and destination directory.


#### plasma_reboot.sh

This script resets the Plasma desktop environment and restarts the system. It was created because there are issues with the ThinkPad T14 when it gets disconnected from one docking station and connected to another docking station. To use the script, run the following command:

```
./plasma_reboot.sh [-f|--force]
```

The `-f` or `--force` option can be used to bypass the confirmation prompt and automatically reboot the system without user input.


### documents

#### optimize_pdf.sh

This script optimizes a specified PDF file by reducing its file size using Ghostscript. To use the script, run the following command:

```
./optimize_pdf.sh [options] <filename>
```

Example:

```
./optimize_pdf.sh input.pdf
```

This command will optimize the PDF file `input.pdf` and create a new, optimized version of the file called `input_opt.pdf` in the same directory.

To specify a custom suffix for the optimized PDF file, use the `-s` or `--suffix` option:

```
./optimize_pdf.sh input.pdf -s _compressed
```

This command will create a new file called `input_compressed.pdf` in the same directory.


#### use_template.sh

This script allows users to create a copy of a selected template folder and rename it with a specified project name.

To use the script, run the following command:

```
./use_template.sh [-h|--help] [-t|--template-dir] [directory] [-d|--destination] [directory]
```

For example:

```
./use_template.sh -t /home/john/templates -d /home/john/projects
```

This command will display a menu of available templates in the `/home/john/templates` directory, and allow the user to select one and specify the name of the new project. The selected template will be copied to the `/home/john/projects` directory with the specified project name.


### professional

#### ref_notes.py

This script provides a command-line interface for creating and managing reference notes for literature.
It uses the `bibtexparser` and `click` libraries, which you will need to install to use the script.

To use the script, you can set the `BIB` environment variable to the path of your BibTeX database file, specify the path to the file as argument if the environment variable is not defined.
You can also set the `EDITOR` environment variable to specify which editor to use for opening the notes files.

To create a notes file for a BibTeX entry, run the script with the BibTeX key of the entry as the argument.
For example, if your BibTeX database contains an entry with the key `my_entry`, you can create a notes file for it with the following command:

```
ref_notes.py my_entry
```

#### rename_cif_files.sh

This script renames all CIF files in a given directory based on their content. The new filename is generated using the chemical composition, space group, and author information extracted from the CIF file.

```
./plasma_reboot.sh [-v]
```

The verbose flag can be set to display messages showing the renaming process.


This will create a notes file for the entry in your `NOTES_DIR` (by default, this is `~/Documents/Notes/Literature`) with the filename `notes_my_entry.md` by inserting metadata originating from the BibTeX database into a template.
Then the notes file is opened in the editor specified by the `EDITOR` environment variable (or `nano` if not set).


### Environmental Variables

The `mk_script.sh` script uses the following environmental variable:

- `EDITOR`: The path to the text editor that will be used to open the new script. If this variable is not set, the script will use `nano` as the default editor.

The `use_template.sh` script uses the following environmental variable:

- `TEMPLATE_ROOT_DIR`: The root directory that contains the templates that the script can use. If this variable is not set, the script will use the current working directory as the default template root directory.

The `ref_notes.py` script uses the following environment variables:

- `BIB`: This variable specifies the path to the BibTeX database file. This variable is optional, and if it is not set a path to a `bib` file has to be specified using the `-b PATH` argument.
- `EDITOR`: This variable specifies the editor to use for opening the notes files. This variable is optional, and if it is not set, the script will use the default value nano.


#### Example of defining an environment variable

```shell
export EDITOR="/usr/bin/vim"
```

This example sets the `EDITOR` environmental variable to the path of the `vim` text editor. This would cause the `mk_script.sh` script to use `vim` as the default editor when creating new scripts.

To set an environmental variable persistently, add the `export` command to your shell profile or `.bashrc` file. For example:

```shell
export EDITOR="/usr/bin/vim"
```

This will set the `EDITOR` variable to the path of the `vim` text editor every time you open a new shell or terminal window. You can also use the `source` command to load the `.bashrc` file and set the environmental variable immediately:

```shell
source ~/.bashrc
```

This will set the `EDITOR` variable in the current shell or terminal window. Consult the documentation for your shell to learn more.


## Contribution

I welcome contributions to this repository! If you have a script that you think would be useful to others, please consider submitting a pull request.

To submit a contribution, follow these steps:

1. Fork the `scripts` repository to your own Codeberg account.
2. Create a new branch in your fork, using a descriptive name that reflects the changes you plan to make (e.g. `add-new-script`, `fix-typo-in-readme`).
3. Make your changes in the new branch, following the guidelines and standards outlined below.
4. Commit your changes, using a clear and concise commit message that describes the changes you have made.
5. Push your changes to your fork on Codeberg.
6. Submit a pull request, requesting that your changes be merged into the `scripts` repository.

The repository maintainers will review your pull request and provide feedback or suggestions if necessary. If your contribution is approved, it will be merged into the `scripts` repository.


## Guidelines and Standards

When contributing to this repository, please follow these guidelines and standards:

- Follow the directory structure and naming conventions used in the repository.
- Use clear and concise comments in your code, to help others understand what your script does and how it works.
- Test your script thoroughly, to ensure that it works as expected and does not contain any errors or bugs.
- Use the `mk_script.sh` script to create new scripts, to ensure that they have the correct structure and use the recommended environmental variables.

Thank you for considering a contribution to this repository! I look forward to reviewing your pull request.


## License

Unless otherwise specified, all files in this repository are licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more information.
