#!/usr/bin/env sh

# Author:  Christian Schreinemachers (Cs137)
# Date:    2023-01-06
# License: MIT


# Script to create or enter a Python project, display the Git status, launch the
# virtual environment, and open an existing vim session. 
#
# The project is expected to be developed with poetry and located in a sub-folder 
# of the one defined by the DEVELOPMENT environment variable, named with its name.
#
# If the project does not exist, it can be created and initialized with poetry.


# Set the project directory
PROJECT_DIR="$DEVELOPMENT/$1"

# Set a sleep counter in order to allow a user to read the git status output
GIT_STATUS_COUNTER=5


# Check if a project name was provided
if [ $# -eq 0 ]
  then
    echo "\033[31mNo project name provided\033[0m"
    exit 1
fi

# Check if the DEVELOPMENT environment variable is set
if [ -z "$DEVELOPMENT" ]
then
  echo "\033[31mDEVELOPMENT environment variable not set\033[0m"
  exit 1
fi

# Check if the poetry command is available
if ! command -v poetry &> /dev/null
then
  echo "\033[31mpoetry command not found\033[0m"
  exit 1
fi
  
  
# Check if the project directory exists
if [ ! -d "$PROJECT_DIR" ]
then
  echo -e "\033[31m\033[1m$1\033[0m\033[31m project does not exist,\033[0m create it? [y/N]"
  read -n 1 -r -s
  # Move the cursor up one line and clear the line
  echo -en "\033[1A" && echo -en "\033[2K"
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    mkdir "$PROJECT_DIR"
    poetry new --src "$PROJECT_DIR"
    printf "\r"
  else
    exit 1
  fi
fi


# Check if the poetry project file exists
if [ ! -f "$PROJECT_DIR/pyproject.toml" ]
then
  echo -e "\033[31m\033[1m$1\033[0m\033[31m is not a poetry project,\033[0m initialise it? [y/N]"
  read -n 1 -r -s
  # Move the cursor up one line and clear the line
  echo -en "\033[1A" && echo -en "\033[2K"
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    poetry init --src "$PROJECT_DIR"
    printf "\r"
  else
    exit 1
  fi
fi


# Change into the project directory
echo -e "\033[32mEntering $1 project directory...\033[0m"
cd "$PROJECT_DIR"


# Check if the project is a Git repository and show the status if so
if [ -d "./.git" ]
then
  echo -n -e "\033[1mgit status:\033[0m "
  git status
  # Display the message and sleep for 1 second before continuing
  while [ $GIT_STATUS_COUNTER -gt -1 ]
  do
    # Clear the current line and move the cursor to the beginning of the line
    printf "\r"
    # Display the message
    printf "(pausing %d s), press Enter to continue..." $GIT_STATUS_COUNTER
    if read -t 1 -n 1 -s
    then
      break
    fi
    GIT_STATUS_COUNTER=$((GIT_STATUS_COUNTER-1))
  done
  # Clear the line and move the cursor to the next line
  printf "\r"
  printf "\n"
else
  git init
fi


# Extract the virtual environment path and start it if it exists
VIRTUALENV_PATH=$(poetry env info --path)
if test -n "$VIRTUALENV_PATH"
then
  echo -e "\033[32mActivating $1 virtual environment...\033[0m"
  source $VIRTUALENV_PATH/bin/activate
else
  echo -e "\033[31mNo Python virtual environment found for \033[1m$1.\033[0m"
fi


# Find the vim session file
project=$(echo "$1" | sed 's/-/_/g')
SESSION_FILE=$(find "$PROJECT_DIR/src/$project/" -name "*.vim" -print -quit)


# Check if a session file was found
if [ -z "$SESSION_FILE" ]
then
  echo -e "\033[31mNo (n)vim session found for \033[1m$1.\033[0m"
  echo -e "To save a (n)vim session, use \033[1m:mksession mysession.vim\033[0m in vim."
else
  # Check if Neovim is installed
  if command -v nvim &> /dev/null
  then
    # Open the session file in Neovim
    nvim -S "$SESSION_FILE"
  elif command -v vim &> /dev/null
  then
    # Open the session file in Vim
    vim -S "$SESSION_FILE"
  else
    # Neovim and Vim are not installed
    echo -e "\033[31mNo (n)vim command available on your system.\033[0m"
  fi
fi



# Deactivate venv if it was activated
if test -n "$VIRTUALENV_PATH"
then
  echo -e "\033[32mDeactivating $1 virtual environment...\033[0m"
  deactivate
fi
