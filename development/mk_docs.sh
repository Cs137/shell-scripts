#!/usr/bin/env sh

# Author:  Christian Schreinemachers (Cs137)
# Date:    2023-01-02
# License: MIT

# This script, 'mk_docs.sh', is used to create the HTML documentation for a python
# package developed with poetry. Since the script uses relative paths, it has to be
# executed from the project directory, which should be named with the package name.
#
# The project is assumed to be structured as shown in the tree underneath. If the 
# 'docs' directory does not exist, it is created and 'sphinx-quickstart' is called
# with 'author', 'name' and 'version' taken from the 'pyproject.toml' file and other
# options can defined at the beginning of the script underneath..
#
#     project_name
#     ├── ...
#     ├── docs
#     │   ├── build
#     │   └── source
#     ├── pyproject.toml
#     └── ...


# ToDo:
# - ensure the theme is set to the config.py as defined
# - allow to add extensions


# Set the list of files to delete when cleaning source files
files_to_delete=("./docs/source/modules.rst" "./docs/source/xrd_tools.rst")

# Set the list of files to delete
language="en"

# Set the theme to be used for the HTML pages (default: 'alabaster')
# theme="html_theme='sphinx_rtd_theme'"
# It should be used like this:  -d "$theme"
# For some reason it does not work and I ignore it for the moment

# Define extensions to be considered by sphinx
#extensions=(autodoc napoleon intersphinx todo)
#ext_args="--extensions ${extensions[*]}"
# It should be used like this:  $ext_args
# For some reason it does not work and I ignore it for the moment


# Check if the 'pyproject.toml' file exists and exit if not.
if [ ! -f "pyproject.toml" ]; then
  echo "Error: You are not in the project directory, no 'pyproject.toml' file was found in the"
  echo "       current working directory (`pwd`)."
  exit 1
fi

# Check if the 'docs' directory exists, create it if not and run sphinx-quickstart
if [ ! -d "docs" ]; then

  # Create the 'docs' directory
  echo " > Creating documentation directory and setting up sphinx..."
  mkdir docs

  # Extract the author, project name, and version from the 'pyproject.toml' file
  author=$(awk '/^authors/ {print $0}' pyproject.toml | sed -e 's/authors = //' -e 's/\[//' -e 's/\]//' -e 's/"//g')
  name=$(awk '/^name/ {print $3}' pyproject.toml | tr -d '"')
  version=$(awk '/^version/ {print $3}' pyproject.toml | tr -d '"')

  # Run the 'sphinx-quickstart' command with the extracted values as options
  poetry run sphinx-quickstart -p "$name" -a "$author" -v "$version" -l "$language" --sep --no-batchfile --makefile -q ./docs
fi

# Create a HTML documentation for the package
echo "Creating documentation for $(basename `pwd`)"

# Delete the existing documentation source files if they exist
echo " > Deleting existing documentation source files..."
for file in "${files_to_delete[@]}"; do
    rm -f "$file"
done


# Build new documentation source files using sphinx-apidoc
echo " > Building new documentation source files..."
poetry run sphinx-apidoc -o ./docs/source ./src/xrd_tools

# Build the new HTML documentation using sphinx-build
echo " > Building new HTML documentation..."
poetry run sphinx-build -b html ./docs/source/ ./docs/build/html
