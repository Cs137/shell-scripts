TOOLS_DIR = ~/.local/bin/tools

link:
	mkdir -p $(TOOLS_DIR)
	stow -v -t $(TOOLS_DIR) --ignore='Makefile' --restow */

unlink:
	stow -v -t $(TOOLS_DIR) --delete */
