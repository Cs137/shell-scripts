#!/usr/bin/env sh

# This script was created by Christian Schreinemachers (Cs137)
# License: MIT

# This script resets the Plasma desktop environment and restarts the system.

# It was created since there are issues with the ThinkPad T14 when it gets dis-
# connected from one docking station and connected to another docking station.

# This is not a good solution at all and I find a way to solve the issue in order 
# to avoid using the script in the future.


# Set the default value for the force option
FORCE=0

# Parse the command-line arguments
while [ $# -gt 0 ]; do
  case "$1" in
    -f|--force)
      FORCE=1
      shift 1
      ;;
    *)
      shift 1
      ;;
  esac
done

# If the force option is not set, display a confirmation prompt
if [ "$FORCE" -eq 0 ]; then
  read -p "This script will reboot the system. Are you sure you want to continue? [y/N] " response
  case "$response" in
    [yY][eE][sS]|[yY])
      # Continue with the script
      ;;
    *)
      # Abort the script
      exit 0
      ;;
  esac
fi

# Kill the plasmashell process
killall plasmashell

# Delete the plasma-org.kde.plasma.desktop-appletsrc file
rm ~/.config/plasma-org.kde.plasma.desktop-appletsrc

# Start the Plasma desktop environment
plasmashell &

# Delete the plasma-org.kde.plasma.desktop-appletsrc file again
rm ~/.config/plasma-org.kde.plasma.desktop-appletsrc

# Restart the system
sudo systemctl reboot
