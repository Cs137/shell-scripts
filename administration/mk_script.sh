#!/usr/bin/env sh

# This script was created by Christian Schreinemachers (Cs137)
# Date: 2022-12-18
# License: MIT

# This script is used to create a new shell script with the specified filename,
# author, license, destination directory, and editor. The default values for the
# author, license, destination, and editor can be set at the beginning of the script
# and can be overridden with command line options. If the DEV_AUTHOR environment
# variable is defined, it will be used as the default value for the author. If the
# EDITOR environment variable is defined, it will be used as the default value for
# the editor. The script will check if the specified editor is available on the
# system and if the destination directory exists. If the specified file already
# exists, the user will be prompted to confirm if they want to overwrite it. The
# new script will be created with a shebang and the specified author, license, and
# the current date. The user will then be able to edit the script with the specified
# editor.


# Define fallback author, license, destination and editor
author="John Doe"
license="MIT"
dest="."
editor="nano"

# Set the author name from the DEV_AUTHOR environment variable if it is defined
if [ -n "$DEV_AUTHOR" ]; then
  author=$DEV_AUTHOR
fi

# Set the editor from the EDITOR environment variable if it is defined
if [ -n "$EDITOR" ]; then
  editor="$EDITOR"
fi

# Set the error handling options
set -e

# Define a function to display the usage information
usage() {
  echo "Usage: $0 [-h] <filename> [-a <author>] [-l <license>] [-d <destination>] [-e <editor>]"
  echo "  -h               Show this help message and exit"
  echo "  -a <author>      The author of the script (default: '$author')"
  echo "  -l <license>     The license of the script (default: '$license')"
  echo "  -d <destination> The destination directory for the script (default: '$dest')"
  echo "  -e <editor>      The editor to write the script (default: '$editor')"
}

# Check if the first argument is an option or an empty string
case "$1" in
  -a|-l|-d|-e|"")
    echo "Error: The first argument must be the filename of the script to be created"
    usage
    exit 1
    ;;
esac

# Display the usage information and exit if the first argument is '-h'
if [ "$1" = "-h" ]; then
  # Print the usage message
  usage
  exit 0
fi

# Set the filename for the new script
filename="$1"
shift

# Parse the optional arguments
while getopts ":a:l:d:e:" opt; do
  case $opt in
    a) author="$OPTARG";;
    l) license="$OPTARG";;
    d) dest="$OPTARG";;
    e) editor="$OPTARG";;
    \?) echo "Error: Invalid option: -$opt" >&2
        usage
        exit 1;;
    :) echo "Error: Option -$opt requires an argument" >&2
       usage
       exit 1;;
  esac
done

# Check if the specified editor is available on the system
if ! command -v "$editor" > /dev/null 2>&1; then
  echo "Error: $editor is not available on this system"
  echo "Please set the \$EDITOR environment variable to the path of a valid editor"
  exit 1
fi

# Check if the specified destination directory exists
if [ ! -d "$dest" ]; then
  echo "Error: destination directory $dest does not exist"
  echo "Please create the directory or choose a different destination"
  exit 1
fi

filepath=$dest/$filename

# Check if the specified file exists already
if [ -e "$filepath" ]; then
  read -r -p "File $filepath already exists. Overwrite? [y/N] " overwrite
  if [ "$overwrite" != "y" ]; then
    echo "Aborting"
    exit 0
  fi
fi

# Create the new script with the specified author and license
cat > "$filepath" <<EOF
#!/usr/bin/env sh

# Author:  $author
# Date:    $(date +%Y-%m-%d)
# License: $license

# Short description of the script's aim


# Add your code here...
EOF

# Make the new script executable
chmod +x "$filepath"

# Open the new script with the specified editor
$editor "$filepath"

echo "Created new script: $filepath"
