#!/usr/bin/env sh

# Author: Christian Schreinemachers (Cs137)
# License: MIT

# This script checks if the user is logged in remotely via SSH and, if so, attempts 
# to attach to an existing screen session or creates a new one, and then exits after 
# a three-second delay.
# To execute the script automatically each time you log in to the remote machine via
# SSH, add it to the `.bash_profile` or `.bashrc` file on the remote machine.


# Check if the user is logged in remotely via SSH
if [[ -z "${STY}" && ! -z "${SSH_CLIENT}" ]]; then
    # Check if there is an existing screen session
    value=$( screen -ls |grep '\<pts.*Detached\>' |wc -l )
    if [ $value -gt 1 ]; then
        # If there is more than one existing session, list the sessions
        screen -ls
    else
        # If there is only one session, attempt to attach to it
        if screen -R; then
            # Display a message and exit after a three-second delay
            echo "3 sec. until the SSH session is terminated."
            echo "^C - to continue working without screen."
            sleep 3
            exit
        else
            # If attaching to the session fails, display an error message
            echo "Failed to attach to screen session. Is screen installed?"
        fi
    fi
fi
