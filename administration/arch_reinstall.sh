#!/usr/bin/env sh

# Author:  Christian Schreinemachers (Cs137)
# Date:    2023-07-24
# License: MIT

# Script to reinstall all packages on a arch system using yay, adopted from
# the example shown there:  https://insrt.uk/post/pacman-reinstall


yay -Sy
packages=($(comm -12 <(yay -Slq|sort -u) <(yay -Qq|sort -u)))
yay -S --overwrite "*" $(echo ${packages[@]} | tr '\n' ' ')
